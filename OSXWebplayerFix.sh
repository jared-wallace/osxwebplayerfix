#!/bin/bash

# You may need to mark this file as executable to run by using
# "chmod +x OSXWebplayerFix.sh"


# Problem set
# Case 1: Both folders present, plugin not in either
notInstalled=1
# Case 2: Both folders present, plugin in system level but not user level
notInUser=2
# Case 3: Both folders present, plugin in user level, but not in system level
notInSystem=3
# Case 4: Both folders present, plugin present in both
notApplicable=4
# Case 5: System folder present, not user level, plugin present in system
systemPresentInstalled=5
# Case 6: System folder present, not user level, plugin not present in system
systemPresentNotInstalled=6
# Case 7: User folder present, not system level, plugin present in user
userPresentInstalled=7
# Case 8: User folder present, not system level, plugin not present in user
userPresentNotInstalled=8
# Case 9: Neither folder is present (not OSX)
notOSX=9
# Case 10: No "Library" folders
noLibrary=10

COLORIZE () {
    case $1 in
    NORMAL)
        COL="\033[0m"
        ;;
    UNDERLINE)
        COL="\e[4m"
        ;;
    WHITE)
        COL="\033[1;28m"
        ;;
    RED)
        COL="\033[0;31m"
        ;;
    GREEN)
        COL="\033[0;32m"
        ;;
    YELLOW)
        COL="\033[0;33m"
        ;;
    BLUE)
        COL="\033[0;34m"
        ;;
    PURPLE)
        COL="\033[0;35m"
        ;;
    L_BLUE)
        COL="\033[0;36m"
        ;;
    esac
    echo -n -e "$COL"
}

UNCOLORIZE () {
    echo -n -e "\033[0m"
}

printf "\033c"
COLORIZE YELLOW
echo "#########################################################################"
echo "# This script is distributed in the hope that it will be useful,        #"
echo "# but WITHOUT ANY WARRANTY; without even the implied warranty of        #"
echo "# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  #"
echo "# IBM will not support the use of this script.                          #"
echo "#########################################################################"
echo
UNCOLORIZE

if [ "$(id -u)" != "0" ]; then
    COLORIZE RED
    echo "Error: This script must be run as root" 1>&2
    UNCOLORIZE
    echo
    exit 1
fi

# Need to check that the script was passed a variable (username)
if [ $# -lt 1 ]; then
    COLORIZE RED
    echo "You must provide your username as an argument. For example:"
    UNCOLORIZE
    echo "$0 JimmyBuffett"
    echo
    exit 1
else
    if ! [ -d /Users/"$1" ]; then
        COLORIZE RED
        echo
        echo "The user \"$1\" does not appear to exist, or at least, the directory"
        echo "/Users/$1 does not exist."
        echo
        echo "Please rectify this, or contact IBM support for assistance."
        UNCOLORIZE
        echo
        exit 1
    fi
fi

userLevel="/Users/$1/Library/Internet Plug-Ins"
userLevelDir="$userLevel/NPRTPluginLL.webplugin"
systemLevel="/Library/Internet Plug-Ins"
systemLevelDir="$systemLevel/NPRTPluginLL.webplugin"
returnCode=0

# Check the system fo the Library Directory
# if they are not present exit the script
if ! [ -d "/Users/$1/Library" ] || ! [ -d "/Library" ];then
exit $noLibrary
fi

# Case 1
if [ -d "$userLevel" ] && [ -d "$systemLevel" ] && ! [ -d "$userLevelDir" ] &&\
    ! [ -d "$systemLevelDir" ]; then
    # Should add some permission checks here, see if we can figure out why the
    # install is failing. One known cause is the user changing their name
    # through the GUI - that screws everything up royally
    COLORIZE GREEN
    echo
    echo "Both user and system level directories are present, but the WebPlayer"
    echo "plugin could not be found in either. Please try to reinstall the"
    echo "WebPlayer Plugin."
    UNCOLORIZE
    returnCode=$notInstalled
# Case 2
elif [ -d "$userLevel" ] && [ -d "$systemLevel" ] &&\
    ! [ -d "$userLevelDir" ] && [ -d "$systemLevelDir" ]; then
    cp -R "$systemLevelDir" "$userLevel"
    COLORIZE GREEN
    echo
    echo "Found the WebPlayer plugin in the system level plugin folder,"
    echo "and copied it to the user level directory."
    UNCOLORIZE
    returnCode=$notInUser
# Case 3
elif [ -d "$userLevel" ] && [ -d "$systemLevel" ] &&\
    [ -d "$userLevelDir" ] && ! [ -d "$systemLevelDir" ]; then
    cp -R "$userLevelDir" "$systemLevel"
    COLORIZE GREEN
    echo
    echo "Found the WebPlayer plugin in the user level directory, and copied"
    echo "it to the system level directory."
    UNCOLORIZE
    returnCode=$notInSystem
# Case 4
elif [ -d "$userLevel" ] && [ -d "$systemLevel" ] &&\
    [ -d "$userLevelDir" ] && [ -d "$systemLevelDir" ];then
    # could maybe do some basic sanity checking, but I have not seen a case
    # where this happened - although perhaps could happen if the user changed
    # their name through the GUI
    COLORIZE GREEN
    echo
    echo "The condition this script was written to fix appears to not be"
    echo "applicable. Please consult IBM support for assistance."
    UNCOLORIZE
    returnCode=$notApplicable
# Case 5
elif ! [ -d "$userLevel" ] && [ -d "$systemLevel" ] &&\
    [ -d "$systemLevelDir" ]; then
    mkdir "$userLevel"
    cp -R "$systemLevelDir" "$userLevel"
    COLORIZE GREEN
    echo
    echo "The user level plugin directory did not exist, so it was created."
    echo "The WebPlayer plugin was discovered at system level, and copied to"
    echo "this new directory."
    UNCOLORIZE
    returnCode=$systemPresentInstalled
# Case 6
elif ! [ -d "$userLevel" ] && [ -d "$systemLevel" ] &&\
    ! [ -d "$systemLevelDir" ]; then
    mkdir "${userLevel}"
    COLORIZE GREEN
    echo
    echo "The user level directory was missing, although the System level"
    echo "directory was present. Unfortunately, the system level plugin"
    echo "directory did not contain the WebPlayer plugin. The user level"
    echo "directory has been created, now please attempt to install the plugin"
    echo "again."
    UNCOLORIZE
    returnCode=$systemPresentNotInstalled
# Case 7
elif [ -d "$userLevel" ] && ! [ -d "$systemLevel" ] &&\
    [ -d "$userLevelDir" ]; then
    mkdir "$systemLevel"
    cp -R "$userLevelDir" "$systemLevel"
    COLORIZE GREEN
    echo
    echo "The user level plugin directory is present, while the system level"
    echo "is not. Discovered the WebPlayer plugin in the user level directory."
    echo "Created the system level directory and copied the WebPlayer plugin"
    echo "to it."
    UNCOLORIZE
    returnCode=$userPresentInstalled
# Case 8
elif [ -d "$userLevel" ] && ! [ -d "$systemLevel" ] &&\
    ! [ -d "$userLevelDir" ]; then
    mkdir "$systemLevel"
    COLORIZE GREEN
    echo
    echo "The system level plugin directory was missing, although the user"
    echo "level directory was present. Neither location contained the WebPlayer"
    echo "plugin. The system level directory has been created, so now please"
    echo "attempt to install the plugin again."
    UNCOLORIZE
    returnCode=$userPresentNotInstalled
# Case 9
elif ! [ -d "$userLevel" ] && ! [ -d "$systemLevel" ]; then
    COLORIZE GREEN
    echo
    echo "Neither plugin directory was present. This system does not appear"
    echo "to be OSX. If in fact this is an OSX system, please consult IBM"
    echo "support for assistance."
    UNCOLORIZE
    returnCode=$notOSX
fi

echo
COLORIZE YELLOW
echo "Script has completed execution"
UNCOLORIZE
exit $returnCode
