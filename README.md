# OSXWebplayerPluginFix

This bash script is designed to fix OSX machines who have issues with IBM's webplayer plugin not being recognized
in the browser.

In OSX, there are two possible locations for a browser plugin to be installed.

1. In the "root" level location, `/Library/Internet Plug-Ins`
2. In the "user" level location, `/Users/{username}/Library/Internet Plug-Ins`

So far, L2 support has seen instances where the user level plugin folder was missing, or had been renamed to
something incorrect, as well as instances where only having the plugin in the "root" level or "user" level
was causing issues. This script attempts to leave the machine in the following state:

1. "root" level plugin folder exists, and has the IBM Webplayer plugin "NPRTPluginLL.webplugin" present
2. "user" level plugin folder exists, and has the IBM Webplayer plugin "NPRTPluginLL.webplugin" present

The various different configurations that are possible are below, as well as the relevant test case name that
handles it.

System Plugin Folder|User Plugin Folder|System Plugin|User Plugin|Test Case
-------|-------|-----|------|----|------
0|0|0|0|notOSX
0|0|0|1|notOSX
0|0|1|0|notOSX
0|0|1|1|notOSX
0|1|0|0|userPresentNotInstalled
0|1|0|1|userPresentInstalled
0|1|1|0|impossible situation
0|1|1|1|impossible situation
1|0|0|0|systemPresentNotInstalled
1|0|0|1|impossible situation
1|0|1|0|systemPresentInstalled
1|0|1|1|impossible situation
1|1|0|0|notInstalled
1|1|0|1|notInSystem
1|1|1|0|notInUser
1|1|1|1|notApplicable

This script and the associated test suite was written by Justin Albano and Jared Wallace, L2 Sametime Software Engineers
