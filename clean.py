#!/usr/bin/python

import os, sys, subprocess

if len(sys.argv) < 2:
    print "You need to pass in the username to use in the test as a command line argument"
    sys.exit()

goodUser = str(sys.argv[1])

# Constants
rootDir = "/Library"
userDir = "/Users/{0}/Library".format(goodUser)
rootPluginDir = "/Library/Internet Plug-Ins"
userPluginDir = "/Users/{0}/Library/Internet Plug-Ins".format(goodUser)
temp1 = "/tmp/rootPluginDir"
temp2 = "/tmp/userPluginDir"

def main():
    subprocess.check_output(["rm", "-rf", rootPluginDir])
    subprocess.check_output(["rm", "-rf", userPluginDir])
    subprocess.check_output(["rm", "-rf", temp1])
    subprocess.check_output(["rm", "-rf", temp2])
    subprocess.check_output(["rm", "-rf", rootDir])
    subprocess.check_output(["rm", "-rf", userDir])
    subprocess.check_output(["mkdir", rootDir])
    subprocess.check_output(["mkdir", userDir])


if __name__ == '__main__':
    main()
