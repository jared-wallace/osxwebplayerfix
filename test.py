#!/usr/bin/python

# This test suite should be run on a clean system!


import unittest, os, sys, argparse, subprocess

if len(sys.argv) < 2:
    print "You need to pass in the username to use in the test as a command line argument"
    sys.exit()

goodUser = str(sys.argv[1])

# Error codes
notInstalled=1
notInUser=2
notInSystem=3
notApplicable=4
systemPresentInstalled=5
systemPresentNotInstalled=6
userPresentInstalled=7
userPresentNotInstalled=8
notOSX = 9
noLibrary = 10

# Constants
rootPluginDir = "/Library/Internet Plug-Ins"
rootPlugin = rootPluginDir + "/NPRTPluginLL.webplugin"
userPluginDir = "/Users/{0}/Library/Internet Plug-Ins".format(goodUser)
userPlugin = userPluginDir + "/NPRTPluginLL.webplugin"
temp1 = "/tmp/rootPluginDir"
temp2 = "/tmp/userPluginDir"


class testOSXPluginFix(unittest.TestCase):

    # Case 9: Neither folder is present (not OSX)
    def testNotOSX(self):
        # If the user has an existing root plugin directory, move it out of harm's way
        if os.path.isdir(rootPluginDir):
            moveDir(rootPluginDir, temp1)
        # If the user has an existing user plugin directory, move it out of harm's way
        if os.path.isdir(userPluginDir):
            moveDir(userPluginDir, temp2)
        # Test
        try:
            debugStatus(9)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, notOSX, "Failed to recognize a non OSX system")

        # Clean up if needed
        if os.path.isdir(temp1):
            moveDir(temp1, rootPluginDir)
        if os.path.isdir(temp2):
            moveDir(temp2, rootPluginDir)

    # Case 8: User folder present, not system level, plugin not present in user
    def testUserPresentNotInstalled(self):
        cleanupNeeded = False
        # Need root level directory gone
        if os.path.isdir(rootPluginDir):
            moveDir(rootPluginDir, temp1)
        # If needed, make a user level plugin directory for testing purposes
        if not os.path.isdir(userPluginDir):
            makeDir(userPluginDir)
            cleanupNeeded = True

        # Test
        try:
            debugStatus(8)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, userPresentNotInstalled, "Failed when system plugin directory is missing, and no plugin is installed anywhere")
        self.assertTrue( os.path.isdir(rootPluginDir) )

        # Clean up
        deleteDir(rootPluginDir)
        if os.path.isdir(temp1):
            moveDir(temp1, rootPluginDir)
        if cleanupNeeded:
            deleteDir(userPluginDir)

    # Case 7: User folder present, not system level, plugin present in user
    def testUserPresentInstalled(self):
        cleanupNeeded = 0
        # Need root level directory gone
        if os.path.isdir(rootPluginDir):
            moveDir(rootPluginDir, temp1)
        # If needed, make a user level plugin directory for testing purposes
        if not os.path.isdir(userPluginDir):
            makeDir(userPluginDir)
            cleanupNeeded = cleanupNeeded + 1
        # If needed, make a fake plugin
        if not os.path.isdir(userPlugin):
            makeDir(userPlugin)
            cleanupNeeded = cleanupNeeded + 2

        # Test
        try:
            debugStatus(7)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, userPresentInstalled, "Failed when system plugin directory is missing, and user plugin is installed")
        self.assertTrue( os.path.isdir(rootPluginDir) )
        self.assertTrue( os.path.isdir(userPlugin) )

        # Clean up
        deleteDir(rootPluginDir)
        if os.path.isdir(temp1):
            moveDir(temp1, rootPluginDir)
        if cleanupNeeded == 3:
            deleteDir(userPluginDir)
        if cleanupNeeded == 2:
            deleteDir(userPlugin)


    # Case 6: System folder present, not user level, plugin not present in system
    def testSystemPresentNotInstalled(self):
        cleanupNeeded = False
        # Need user level directory gone
        if os.path.isdir(userPluginDir):
            moveDir(userPluginDir, temp1)
        # If needed, make a root level plugin directory for testing purposes
        if not os.path.isdir(rootPluginDir):
            makeDir(rootPluginDir)
            cleanupNeeded = True

        # Test
        try:
            debugStatus(6)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, systemPresentNotInstalled, "Failed when user plugin directory is missing, and no plugin is installed anywhere")
        self.assertTrue( os.path.isdir(userPluginDir) )

        # Clean up
        deleteDir(userPluginDir)
        if os.path.isdir(temp1):
            moveDir(temp1, userPluginDir)
        if cleanupNeeded:
            deleteDir(rootPluginDir)


    # Case 5: System folder present, not user level, plugin present in system
    def testSystemPresentInstalled(self):
        cleanupNeeded = 0
        # Need user level directory gone
        if os.path.isdir(userPluginDir):
            moveDir(userPluginDir, temp1)
        # If needed, make a root level plugin directory for testing purposes
        if not os.path.isdir(rootPluginDir):
            makeDir(rootPluginDir)
            cleanupNeeded = cleanupNeeded + 1
        # If needed, make a fake plugin
        if not os.path.isdir(rootPlugin):
            makeDir(rootPlugin)
            cleanupNeeded = cleanupNeeded + 2

        # Test
        try:
            debugStatus(5)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, systemPresentInstalled, "Failed when user plugin directory is missing, and root plugin is installed")
        self.assertTrue( os.path.isdir(userPluginDir) )
        self.assertTrue( os.path.isdir(userPlugin) )

        # Clean up
        deleteDir(userPluginDir)
        if os.path.isdir(temp1):
            moveDir(temp1, userPluginDir)
        if cleanupNeeded == 3:
            deleteDir(rootPluginDir)
        if cleanupNeeded == 2:
            deleteDir(rootPlugin)


    # Case 4: Both folders present, plugin present in both
    def testNotApplicable(self):
        cleanupNeeded = 0
        # If needed, make a root level plugin directory for testing purposes
        if not os.path.isdir(rootPluginDir):
            makeDir(rootPluginDir)
            cleanupNeeded = cleanupNeeded + 1
        # If needed, make a fake plugin
        if not os.path.isdir(rootPlugin):
            makeDir(rootPlugin)
            cleanupNeeded = cleanupNeeded + 2
        # If needed, make a user level plugin directory for testing purposes
        if not os.path.isdir(userPluginDir):
            makeDir(userPluginDir)
            cleanupNeeded = cleanupNeeded + 4
        # If needed, make a fake plugin
        if not os.path.isdir(userPlugin):
            makeDir(userPlugin)
            cleanupNeeded = cleanupNeeded + 8

        # Test
        try:
            debugStatus(4)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, notApplicable, "Failed when not applicable")

        # Clean up
        if cleanupNeeded == 2:
            deleteDir(rootPlugin)
        if cleanupNeeded == 3:
            deleteDir(rootPluginDir)
        if cleanupNeeded == 8:
            deleteDir(userPlugin)
        if cleanupNeeded == 11:
            deleteDir(rootPluginDir)
            deleteDir(userPlugin)
        if cleanupNeeded == 12:
            deleteDir(userPluginDir)
        if cleanupNeeded == 14:
            deleteDir(userPluginDir)
            deleteDir(rootPlugin)
        if cleanupNeeded == 15:
            deleteDir(userPluginDir)
            deleteDir(rootPluginDir)


    # Case 3: Both folders present, plugin in user level, but not in system level
    def testNotInSystem(self):
        cleanupNeeded = 0
        # If they have a root plugin right now, move it out of harm's way
        if os.path.isdir(rootPlugin):
            moveDir(rootPlugin, temp1)
        # otherwise, check to see if they have a root plugins folder
        elif not os.path.isdir(rootPluginDir):
            makeDir(rootPluginDir)
            cleanupNeeded = cleanupNeeded + 1
        # Check to see if they have user level plugin folder
        if not os.path.isdir(userPluginDir):
            makeDir(userPluginDir)
            makeDir(userPlugin)
            cleanupNeeded = cleanupNeeded + 2
        elif not os.path.isdir(userPlugin):
            makeDir(userPlugin)
            cleanupNeeded = cleanupNeeded + 4

        # Test
        try:
            debugStatus(3)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, notInSystem, "Failed when not in system")
        self.assertTrue( os.path.isdir(rootPlugin) )

        # Cleanup
        if cleanupNeeded == 1:
            deleteDir(rootPluginDir)
        if cleanupNeeded == 2:
            deleteDir(userPluginDir)
            deleteDir(userPlugin)
        if cleanupNeeded == 3:
            deleteDir(rootPluginDir)
            deleteDir(userPluginDir)
        if cleanupNeeded == 4:
            deleteDir(userPlugin)
        if cleanupNeeded == 5:
            deleteDir(rootPluginDir)
            deleteDir(userPlugin)


    # Case 2: Both folders present, plugin in system level but not user level
    def testNotInUser(self):
        cleanupNeeded = 0
        # If they have a user plugin right now, move it out of harm's way
        if os.path.isdir(userPlugin):
            moveDir(userPlugin, temp1)
        # otherwise, check to see if they have a user plugins folder
        elif not os.path.isdir(userPluginDir):
            makeDir(userPluginDir)
            cleanupNeeded = cleanupNeeded + 1
        # Check to see if they have root level plugin folder
        if not os.path.isdir(rootPluginDir):
            makeDir(rootPluginDir)
            makeDir(rootPlugin)
            cleanupNeeded = cleanupNeeded + 2
        elif not os.path.isdir(rootPlugin):
            makeDir(rootPlugin)
            cleanupNeeded = cleanupNeeded + 4

        # Test
        try:
            debugStatus(2)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, notInUser, "Failed when not in system")
        self.assertTrue( os.path.isdir(userPlugin) )

        # Cleanup
        if cleanupNeeded == 1:
            deleteDir(userPluginDir)
        if cleanupNeeded == 2:
            deleteDir(rootPluginDir)
        if cleanupNeeded == 3:
            deleteDir(rootPluginDir)
            deleteDir(userPluginDir)
        if cleanupNeeded == 4:
            deleteDir(rootPlugin)
        if cleanupNeeded == 5:
            deleteDir(userPluginDir)
            deleteDir(rootPlugin)

    # Case 1: Both folders present, plugin not in either
    def testNotInstalled(self):
        cleanupNeeded = 0
        # If they have a user plugin right now, move it out of harm's way
        if os.path.isdir(userPlugin):
            moveDir(userPlugin, temp1)
        # If they have a root plugin right now, move it out of harm's way
        if os.path.isdir(rootPlugin):
            moveDir(rootPlugin, temp1)
        # Make user plugin directory if needed
        if not os.path.isdir(userPluginDir):
            makeDir(userPluginDir)
            cleanupNeeded = cleanupNeeded + 1
        # Make root plugin directory if needed
        if not os.path.isdir(rootPluginDir):
            makeDir(rootPluginDir)
            cleanupNeeded = cleanupNeeded + 2

        # Test
        try:
            debugStatus(1)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, notInstalled, "Failed when not in system")

        # Cleanup
        if cleanupNeeded == 1:
            deleteDir(userPluginDir)
        if cleanupNeeded == 2:
            deleteDir(rootPluginDir)
        if cleanupNeeded == 3:
            deleteDir(userPluginDir)
            deleteDir(rootPluginDir)


    def testNoLibrary(self):
        cleanupNeeded = 0
        debugStatus(10)
        rootDir = "/Library"
        userDir = "/Users/{0}/Library".format(goodUser)
        if os.path.isdir(rootDir):
            moveDir(rootDir, temp1)
        if os.path.isdir(userDir):
            moveDir(userDir, temp2)
        try:
            debugStatus(10)
            subprocess.check_call(["bash", "./OSXWebplayerFix.sh", goodUser])
        except subprocess.CalledProcessError as e1:
            self.assertEqual(e1.returncode, noLibrary, "Failed when Library not present")
        if os.path.isdir(temp1):
            moveDir(temp1, rootDir)
        if os.path.isdir(temp2):
            moveDir(temp2, userDir)
        debugStatus(10)



# Common setup and cleanup utility functions
def deleteDir(path):
    try:
        subprocess.check_output(["rm", "-rf", path])
    except subprocess.CalledProcessError as e1:
        print "{0} {1}".format(e1.returncode, e1.output)
    except:
        print "Failed to delete \"{0}\"".format(path)

def makeDir(path):
    try:
        subprocess.check_output(["mkdir", path])
    except subprocess.CalledProcessError as e1:
        print "{0} {1}".format(e1.returncode, e1.output)
    except:
        print "Failed to make \"{0}\"".format(path)

def moveDir(path1, path2):
    try:
        subprocess.check_output(["rm", "-rf", path2])
        subprocess.check_output(["mv", path1, path2])
    except subprocess.CalledProcessError as e1:
        print "{0} {1}".format(e1.returncode, e1.output)
    except:
        print "Failed to move \"{0}\" to \"{1}\"".format(path1, path2)

def debugStatus(case):
    print "Case # {0}".format(case)
    if os.path.isdir(rootPluginDir):
        print "Root folder exists"
    if os.path.isdir(rootPlugin):
        print "Root plugin exists"
    if os.path.isdir(userPluginDir):
        print "User folder exists"
    if os.path.isdir(userPlugin):
        print "User plugin exists"
    if os.path.isdir(temp1):
        print "Temp1 exists"
    if os.path.isdir(temp2):
        print "Temp2 exists"

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('username', default='macadmin')
    parser.add_argument('unittest_args', nargs='*')

    args = parser.parse_args()
    sys.argv[1:] = args.unittest_args
    unittest.main()
